﻿// <copyright file="TriangleSorterTests.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task3.Tests
{
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class TriangleSorterTests
    {
        [TestMethod]
        public void GetSortedTrianglesTest()
        {
            var triangles = new List<Triangle>
                                {
                                    new Triangle("n1", 5, 6, 5),
                                    new Triangle("n2", 8, 8, 8),
                                    new Triangle("n4", 7, 6, 5),
                                    new Triangle("n3", 10, 12, 15)
                                };

            var sortedTriangles = TriangleSorter.GetSortedTriangles(triangles);

            var expected = triangles.OrderBy(triangle => triangle.Square);

            CollectionAssert.AreEqual(expected.ToArray(), sortedTriangles.ToArray());
        }
    }
}
﻿// <copyright file="TriangleTests.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task3.Tests
{
    using System;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class TriangleTests
    {
        [TestMethod]
        public void TriangleTest_Impossible_Exception()
        {
            Assert.ThrowsException<ArgumentException>(() => new Triangle("someName", 2, 2, 50));
        }
    }
}
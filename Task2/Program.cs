﻿// <copyright file="Program.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

// Есть два конверта со сторонами(a, b) и(c, d) определить, можно ли один конверт вложить в другой.
// Программа должна обрабатывать ввод чисел с плавающей точкой.
// Программа спрашивает у пользователя размеры конвертов по одному параметру за раз.
// После каждого подсчёта программа спрашивает у пользователя хочет ли он продолжить.
// Если пользователь ответит “y” или “yes” (без учёта регистра), программа продолжает работу сначала,
// в противном случае – завершает выполнение.

namespace Task2
{
    using System;

    using InputHandling;

    /// <summary>
    ///     Contains Main.
    /// </summary>
    public class Program
    {
        /// <summary>
        ///     Defines the entry point of the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public static void Main(string[] args)
        {
            InputHandlerYesNo handlerYN = new InputHandlerYesNo("Do you want continue?");
            do
            {
                Console.WriteLine("First envelope");
                Envelope envelope1 = GetEnvelopeFromInput();
                Console.WriteLine("Second envelope");
                Envelope envelope2 = GetEnvelopeFromInput();

                if (envelope1.CheckCanContain(envelope2))
                {
                    Console.WriteLine("First envelope can contain second");
                }
                else
                {
                    Console.WriteLine("First envelope can'n contain second");
                }

                if (envelope2.CheckCanContain(envelope1))
                {
                    Console.WriteLine("Second envelope can contain first");
                }
                else
                {
                    Console.WriteLine("Second envelope can't contain first");
                }
            }
            while (handlerYN.GetInput());
        }

        /// <summary>
        ///     Gets the envelope from input.
        /// </summary>
        /// <returns>Returns new envelope with entered params.</returns>
        private static Envelope GetEnvelopeFromInput()
        {
            InputHandlerDouble inputHandler = new InputHandlerDouble("Width:")
                                                  {
                                                      CanBeNegative = false,
                                                      MinimumValue = 1
                                                  };

            double width = inputHandler.GetInput();
            inputHandler.InvitationText = "Height";
            double height = inputHandler.GetInput();

            return new Envelope(width, height);
        }
    }
}
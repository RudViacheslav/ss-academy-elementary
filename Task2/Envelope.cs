﻿// <copyright file="Envelope.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task2
{
    using System;

    /// <summary>
    ///     Represent envelope.
    /// </summary>
    public class Envelope
    {
        /// <summary>
        ///     The minimum size of envelope side.
        /// </summary>
        private const double MinSize = 0.1;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Envelope" /> class.
        /// </summary>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        public Envelope(double width, double height)
        {
            if (width < MinSize || height < MinSize)
            {
                throw new ArgumentException(string.Format("Width and height must bee greater than {0}", MinSize));
            }

            Width = width;
            Height = height;
        }

        /// <summary>
        ///     Gets the height.
        /// </summary>
        /// <value>
        ///     The height.
        /// </value>
        public double Height { get; }

        /// <summary>
        ///     Gets the width.
        /// </summary>
        /// <value>
        ///     The width.
        /// </value>
        public double Width { get; }

        /// <summary>
        ///     Checks whether this envelope can contain other.
        /// </summary>
        /// <param name="other">The other envelope.</param>
        /// <returns>Returns true if envelpe can contain other.</returns>
        public bool CheckCanContain(Envelope other)
        {
            // if this can contain other envelope without any rotation or with rotation 90 degrees
            if (Height >= other.Height &&
                Width >= other.Width ||
                Height >= other.Width &&
                Width >= other.Height)
            {
                return true;
            }

            double p = Math.Max(other.Width, other.Height);
            double q = Math.Min(other.Width, other.Height);

            double a = Math.Max(Width, Height);
            double b = Math.Min(Width, Height);

            // if both fugures is not quadratic and long formula returns true
            if (!(Height != Width && other.Height != other.Width) &&

                // Documentation/1.png
                p > a && b >= (2 * p * q * a + (p * p - q * q) * Math.Sqrt(p * p + q * q - a * a)) / (p * p + q * q))
            {
                return true;
            }

            return false;
        }
    }
}
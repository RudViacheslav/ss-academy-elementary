﻿// <copyright file="Program.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task6
{
    using System;

    using InputHandling;

    // Счастливые билеты.
    // Есть 2 способа подсчёта счастливых билетов:
    // 1. Московский — если на автобусном билете напечатано шестизначное число,
    // и сумма первых трёх цифр равна сумме последних трёх, то этот билет считается счастливым.
    // 2. Ленинградский, или Питерский — если сумма чётных цифр билета равна сумме нечётных цифр билета,
    // то билет считается счастливым.
    // Задача:
    // Номер билета - шестизначное число. Нужно написать консольное приложение,
    // которое может посчитать количество счастливых билетов. Для выбора алгоритма подсчёта читается текстовый файл.
    // Путь к текстовому файлу задаётся в консоли после запуска программы. Индикаторы алгоритмов:
    // 1 - слово 'Moskow'
    // 2 - слово 'Piter'
    // После задания всех необходимых параметров,
    // программа в консоль должна вывести количество счастливых билетов для указанного способа подсчёта.

    /// <summary>
    ///     Contains Main.
    /// </summary>
    public class Program
    {
        /// <summary>
        ///     Defines the entry point of the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public static void Main(string[] args)
        {
            InputHandlerTextFromFile fileHandler = new InputHandlerTextFromFile("Type path to file with data:");
            InputHandlerYesNo ynHandler = new InputHandlerYesNo("Enter another value?");
            do
            {
                string way = fileHandler.GetInput(out string path);
                if (way == "Moskow")
                {
                    Console.WriteLine("Selected Moskow way");
                    Console.WriteLine(TicketChecker.GetCountOfHappyTickets(TicketChecker.CheckType.Moskow));
                }
                else if (way == "Piter")
                {
                    Console.WriteLine("Selected Piter way");
                    Console.WriteLine(TicketChecker.GetCountOfHappyTickets(TicketChecker.CheckType.Piter));
                }
                else
                {
                    Console.WriteLine("File contain wrong data.");
                }
            }
            while (ynHandler.GetInput());

            // Console.WriteLine(TicketChecker.GetCountOfHappyTickets(TicketChecker.CheckType.Moskow));
            // Console.WriteLine(TicketChecker.GetCountOfHappyTickets(TicketChecker.CheckType.Piter));
            Console.ReadLine();
        }
    }
}
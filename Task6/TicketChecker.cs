﻿// <copyright file="TicketChecker.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task6
{
    using System;
    using System.Linq;

    /// <summary>
    ///     Provide methods to get happy tickets.
    /// </summary>
    public static class TicketChecker
    {
        /// <summary>
        ///     The maximum ticket value
        /// </summary>
        private const int MaxTicketValue = 999_999;

        /// <summary>
        ///     Count of digits in ticket number.
        /// </summary>
        private const int SizeOfTicketNumber = 6;

        /// <summary>
        ///     Delegate for checking algorithm.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <returns>Returns true if number is happy.</returns>
        private delegate bool CheckHappinesDelegate(int number);

        public enum CheckType
        {
            Moskow,
            Piter
        }

        /// <summary>
        ///     Checks is it happy ticket by Moskow algotithm.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <returns>Returns true or false whether it happy.</returns>
        /// <exception cref="ArgumentOutOfRangeException">Numer is greater than max value.</exception>
        public static bool CheckIsItHappyMoskow(int number)
        {
            CheckAndThrowIfNumberGreater(number);
            var digits = GetArrayOfDigits(number);
            int firstSum = digits[0] + digits[1] + digits[2];
            int lastSum = digits[3] + digits[4] + digits[5];

            return firstSum == lastSum;
        }

        /// <summary>
        ///     Checks is it happy ticket by Piter algotithm.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <returns>Returns true or false whether it happy.</returns>
        /// <exception cref="ArgumentException">Numer is greater than max value.</exception>
        public static bool CheckIsItHappyPiter(int number)
        {
            CheckAndThrowIfNumberGreater(number);
            var digits = GetArrayOfDigits(number);

            int evenSum = digits[0] + digits[2] + digits[4];
            int oddSum = digits[1] + digits[3] + digits[5];

            return evenSum == oddSum;
        }

        /// <summary>
        ///     Gets the count of happy tickets.
        /// </summary>
        /// <param name="checkType">Type of the check.</param>
        /// <returns>Returns the count of happy tickets.</returns>
        public static int GetCountOfHappyTickets(CheckType checkType)
        {
            CheckHappinesDelegate algorithm;
            if (checkType == CheckType.Moskow)
            {
                algorithm = CheckIsItHappyMoskow;
            }
            else
            {
                algorithm = CheckIsItHappyPiter;
            }

            return CountHappyTickets(algorithm);
        }

        /// <summary>
        ///     Checks the and throw if number greater.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <exception cref="System.ArgumentOutOfRangeException"></exception>
        private static void CheckAndThrowIfNumberGreater(int number)
        {
            if (number > MaxTicketValue)
            {
                throw new ArgumentOutOfRangeException("Numer is greater than max value.");
            }

            if (number < 0)
            {
                throw new ArgumentOutOfRangeException("Numer is negative.");
            }
        }

        /// <summary>
        ///     Returns count of happy digits.
        /// </summary>
        /// <param name="checkingAlgorithm">The checking algorithm.</param>
        /// <returns>Returns count of happy digits.</returns>
        private static int CountHappyTickets(CheckHappinesDelegate checkingAlgorithm)
        {
            int count = 1;
            for (int i = 0; i < MaxTicketValue; i++)
            {
                if (checkingAlgorithm(i))
                {
                    count++;
                }
            }

            return count;
        }

        /// <summary>
        ///     Gets the array of digits.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <returns>Returns the array of digits.</returns>
        private static int[] GetArrayOfDigits(int number)
        {
            var digits = new int[SizeOfTicketNumber];

            for (int i = SizeOfTicketNumber - 1; i >= 0; i--)
            {
                digits[i] = number % 10;
                number /= 10;
            }

            return digits.ToArray();
        }
    }
}
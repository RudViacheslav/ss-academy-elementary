﻿// <copyright file="Triangle.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task3
{
    using System;

    /// <summary>
    ///     Represent a triangle with his sides.
    /// </summary>
    public class Triangle
    {
        /// <summary>
        ///     The sides.
        /// </summary>
        private readonly double[] sides = new double[3];

        /// <summary>
        ///     Initializes a new instance of the <see cref="Triangle" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="side1">The side1.</param>
        /// <param name="side2">The side2.</param>
        /// <param name="side3">The side3.</param>
        public Triangle(string name, double side1, double side2, double side3)
        {
            Name = name;
            sides[0] = side1;
            sides[1] = side2;
            sides[2] = side3;

            double sides12 = side1 + side2;
            double sides13 = side1 + side3;
            double sides23 = side2 + side3;

            if (sides12 < side3 ||
                sides13 < side2 ||
                sides23 < side1)
            {
                throw new ArgumentException("It's impossible triangle.");
            }
        }

        /// <summary>
        ///     Gets the name.
        /// </summary>
        /// <value>
        ///     The name.
        /// </value>
        public string Name { get; }

        /// <summary>
        ///     Gets the square.
        /// </summary>
        /// <value>
        ///     The square.
        /// </value>
        public double Square
        {
            get
            {
                double p = 0;
                foreach (double side in sides)
                {
                    p += side;
                }

                return Math.Sqrt(p * (p - sides[0]) * (p - sides[1] * (p - sides[2])));
            }
        }
    }
}
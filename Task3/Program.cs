﻿// <copyright file="Program.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task3
{
    using System;
    using System.Collections.Generic;

    using InputHandling;

    // Разработать консольную программу, выполняющую вывод треугольников в порядке убывания их площади.
    // После добавления каждого нового треугольника программа спрашивает, хочет ли пользователь добавить ещё один.
    // Если пользователь ответит “y” или “yes” (без учёта регистра), программа попросит ввести данные для ещё одного треугольника,
    // в противном случае – выводит результат в консоль.

    // •    Расчёт площади треугольника должен производится по формуле Герона.
    // •    Каждый треугольник определяется именем и длинами его сторон. 
    // Формат ввода (разделитель - запятая): 
    // <имя>, <длина стороны>, <длина стороны>, <длина стороны>
    // •    Приложение должно обрабатывать ввод чисел с плавающей точкой.
    // •    Ввод должен быть нечувствителен к регистру, пробелам, табам.
    // •    Вывод данных должен быть следующем примере:
    // ============= Triangles list: ===============
    // 1. [Triangle first]: 17.23 сm
    // 2. [Triangle 22]: 13 cm
    // 3. [Triangle 1]: 1.5 cm

    /// <summary>
    ///     Contains Main.
    /// </summary>
    public class Program
    {
        /// <summary>
        ///     Defines the entry point of the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public static void Main(string[] args)
        {
            InputHandlerArrayOfDouble handler = new InputHandlerArrayOfDouble("Side ");
            InputHandlerYesNo handlerYN = new InputHandlerYesNo("Enter another one?");
            var triangles = new List<Triangle>();

            do
            {
                Console.WriteLine("Name");
                string name = Console.ReadLine();
                var values = handler.GetInput(3);
                Triangle newTriangle;
                try
                {
                    newTriangle = new Triangle(name, values[0], values[1], values[2]);
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Try again.");
                    continue;
                }

                triangles.Add(newTriangle);

                var sorted = TriangleSorter.GetSortedTriangles(triangles);
                foreach (Triangle triangle in triangles)
                {
                    Console.WriteLine("Name: {0},\tSquare: {1}", triangle.Name, triangle.Square);
                }
            }
            while (handlerYN.GetInput());
        }
    }
}
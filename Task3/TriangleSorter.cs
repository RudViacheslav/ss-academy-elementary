﻿// <copyright file="TriangleSorter.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task3
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    ///     Sorts the triangles.
    /// </summary>
    public static class TriangleSorter
    {
        /// <summary>
        ///     Gets the sorted triangles.
        /// </summary>
        /// <param name="triangles">The triangles.</param>
        /// <returns>Returns IOrderedEnumerable&lt;Triangle&gt;.</returns>
        public static IOrderedEnumerable<Triangle> GetSortedTriangles(IEnumerable<Triangle> triangles)
        {
            return triangles.OrderBy(triangle => triangle.Square);
        }
    }
}
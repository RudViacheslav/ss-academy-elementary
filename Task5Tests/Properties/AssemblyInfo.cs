// <copyright file="AssemblyInfo.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Task5Tests")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Task5Tests")]
[assembly: AssemblyCopyright("Copyright ©  2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("da1cb22c-4b27-4864-a7e7-f8da20e4cbfe")]

// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
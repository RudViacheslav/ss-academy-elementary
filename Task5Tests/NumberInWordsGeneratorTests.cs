﻿// <copyright file="NumberInWordsGeneratorTests.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task5.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class NumberInWordsGeneratorTests
    {
        [TestMethod]
        public void Check_0()
        {
            Assert.AreEqual("ноль", new NumberInWordsGenerator().GenerateString(0));
        }

        [TestMethod]
        public void Check_2548()
        {
            string str = new NumberInWordsGenerator().GenerateString(2548);
            Assert.AreEqual("две тысячи пятьсот сорок восемь", str);
        }

        [TestMethod]
        public void Check_5()
        {
            Assert.AreEqual("пять", new NumberInWordsGenerator().GenerateString(5));
        }

        /// <summary>
        ///     Test completes if no exceptions thorws.
        /// </summary>
        [TestMethod]
        public void GenerateAllNumbersNoException()
        {
            NumberInWordsGenerator generator = new NumberInWordsGenerator();
            for (int i = 0; i <= NumberInWordsGenerator.MaxValue; i++)
            {
                generator.GenerateString(i);
            }
        }
    }
}
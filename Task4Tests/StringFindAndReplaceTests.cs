﻿// <copyright file="StringFindAndReplaceTests.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task4.Tests
{
    using System.Collections.Generic;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class StringFindAndReplaceTests
    {
        [TestMethod]
        public void ChangeCapitalizationTest()
        {
            string str = "aaa";
            string expected = "AaA";
            bool[] capitalization = { true, false, true };
            string actural = StringFindAndReplace.ChangeCapitalization(str, capitalization);
            Assert.AreEqual(expected, actural);
        }

        [TestMethod]
        public void GetAllOccurencesTest()
        {
            string str = "ounoumnyiOu ou y oU";
            var expected = new List<Occurrence>
                               {
                                   new Occurrence(0, 1, new[] { false, false }),
                                   new Occurrence(3, 4, new[] { false, false }),
                                   new Occurrence(9, 10, new[] { true, false }),
                                   new Occurrence(12, 13, new[] { false, false }),
                                   new Occurrence(17, 18, new[] { false, true })
                               };

            var actual = StringFindAndReplace.GetAllOccurrences(str, "ou");

            if (expected.Count != actual.Count)
            {
                Assert.Fail("Count of actual occurences not same as expected count");
            }

            for (int i = 0; i < actual.Count; i++)
            {
                if (actual[i].Start + 2 != expected[i].Start + 2 ||
                    actual[i].Start != expected[i].Start)
                {
                    Assert.Fail();
                }
                else
                {
                    CollectionAssert.AreEqual(expected[i].Capitalization, actual[i].Capitalization);
                }
            }
        }

        [TestMethod]
        public void GetCapitalizationOfStringTest()
        {
            string str = "UaYiiNM";
            bool[] expected = { true, false, true, false, false, true, true };

            var actual = StringFindAndReplace.GetCapitalization(str);

            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ReplaceKeepCapitalizationTest_BiggerLength()
        {
            string str = "aa bbuuAa";
            string actual = StringFindAndReplace.ReplaceKeepCapitalization(str, "aa", "bbb");
            string expected = "bbb bbuuBbb";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ReplaceKeepCapitalizationTest_SameLength()
        {
            string str = "aa bbuuAa";
            string actual = StringFindAndReplace.ReplaceKeepCapitalization(str, "aa", "bb");
            string expected = "bb bbuuBb";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ReplaceKeepCapitalizationTest_SmallerLength()
        {
            string str = "aa bbuuAa";
            string actual = StringFindAndReplace.ReplaceKeepCapitalization(str, "aa", "b");
            string expected = "b bbuuB";

            Assert.AreEqual(expected, actual);
        }
    }
}
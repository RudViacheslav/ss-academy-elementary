﻿// <copyright file="ChessboardTests.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task1.Tests
{
    using System;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class ChessboardTests
    {
        [TestMethod]
        public void ChessboardInitializeTest_SizeLower_Than2_Exception()
        {
            Assert.ThrowsException<ArgumentException>(() => new Chessboard(0, 0));
        }

        [TestMethod]
        public void GetCharChessboardIflessTest_4x4()
        {
            Chessboard chessboard = new Chessboard(4, 4);

            var charChessboard = chessboard.GetCharChessboardIfless();

            var expectedChessboard = new char[4, 4];
            expectedChessboard[0, 0] = ' ';
            expectedChessboard[0, 1] = '#';
            expectedChessboard[0, 2] = ' ';
            expectedChessboard[0, 3] = '#';

            expectedChessboard[1, 0] = '#';
            expectedChessboard[1, 1] = ' ';
            expectedChessboard[1, 2] = '#';
            expectedChessboard[1, 3] = ' ';

            expectedChessboard[2, 0] = ' ';
            expectedChessboard[2, 1] = '#';
            expectedChessboard[2, 2] = ' ';
            expectedChessboard[2, 3] = '#';

            expectedChessboard[3, 0] = '#';
            expectedChessboard[3, 1] = ' ';
            expectedChessboard[3, 2] = '#';
            expectedChessboard[3, 3] = ' ';

            CollectionAssert.AreEqual(expectedChessboard, charChessboard);
        }

        [TestMethod]
        public void GetCharChessboardTest_4x4()
        {
            Chessboard chessboard = new Chessboard(4, 4);

            var charChessboard = chessboard.GetCharChessboard();

            var expectedChessboard = new char[4, 4];
            expectedChessboard[0, 0] = ' ';
            expectedChessboard[0, 1] = '#';
            expectedChessboard[0, 2] = ' ';
            expectedChessboard[0, 3] = '#';

            expectedChessboard[1, 0] = '#';
            expectedChessboard[1, 1] = ' ';
            expectedChessboard[1, 2] = '#';
            expectedChessboard[1, 3] = ' ';

            expectedChessboard[2, 0] = ' ';
            expectedChessboard[2, 1] = '#';
            expectedChessboard[2, 2] = ' ';
            expectedChessboard[2, 3] = '#';

            expectedChessboard[3, 0] = '#';
            expectedChessboard[3, 1] = ' ';
            expectedChessboard[3, 2] = '#';
            expectedChessboard[3, 3] = ' ';

            CollectionAssert.AreEqual(expectedChessboard, charChessboard);
        }
    }
}
﻿// <copyright file="TicketCheckerTests.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task6.Tests
{
    using System;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class TicketCheckerTests
    {
        [TestMethod]
        public void CheckIsItHappyMoskowTest_minus5_True()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => TicketChecker.CheckIsItHappyMoskow(-5));
        }

        [TestMethod]
        public void CheckIsItHappyMoskowTest_000000_True()
        {
            Assert.AreEqual(true, TicketChecker.CheckIsItHappyMoskow(000_000));
        }

        [TestMethod]
        public void CheckIsItHappyMoskowTest_1000000_Exception()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => TicketChecker.CheckIsItHappyMoskow(1_000_000));
        }

        [TestMethod]
        public void CheckIsItHappyMoskowTest_333000_False()
        {
            Assert.AreEqual(false, TicketChecker.CheckIsItHappyMoskow(333_000));
        }

        [TestMethod]
        public void CheckIsItHappyMoskowTest_333333_True()
        {
            Assert.AreEqual(true, TicketChecker.CheckIsItHappyMoskow(333_333));
        }

        [TestMethod]
        public void CheckIsItHappyPiterTest_000000_True()
        {
            Assert.AreEqual(true, TicketChecker.CheckIsItHappyPiter(723624));
        }

        [TestMethod]
        public void CheckIsItHappyPiterTest_1000000_Exception()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => TicketChecker.CheckIsItHappyPiter(1_000_000));
        }

        [TestMethod]
        public void CheckIsItHappyPiterTest_7236248_True()
        {
            Assert.AreEqual(true, TicketChecker.CheckIsItHappyPiter(723624));
        }

        [TestMethod]
        public void CheckIsItHappyPiterTest_7236249_False()
        {
            Assert.AreEqual(false, TicketChecker.CheckIsItHappyPiter(703620));
        }

        [TestMethod]
        public void GetCountOfHappyTicketsTest_Moskow()
        {
            Assert.AreEqual(55252, TicketChecker.GetCountOfHappyTickets(TicketChecker.CheckType.Moskow));
        }

        [TestMethod]
        public void GetCountOfHappyTicketsTest_Piter()
        {
            Assert.AreEqual(55252, TicketChecker.GetCountOfHappyTickets(TicketChecker.CheckType.Piter));
        }
    }
}
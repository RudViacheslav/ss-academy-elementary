// <copyright file="AssemblyInfo.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Task6Tests")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Task6Tests")]
[assembly: AssemblyCopyright("Copyright ©  2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("ae5a269d-8ac5-4877-8b8e-3f720e4fc435")]

// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
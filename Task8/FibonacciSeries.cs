﻿// <copyright file="FibonacciSeries.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task8
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    ///     Contain methods for fibonacciSeries.
    /// </summary>
    public static class FibonacciSeries
    {
        /// <summary>
        ///     Gets the series in range.
        /// </summary>
        /// <param name="lowerBoundary">The lower boundary.</param>
        /// <param name="upperBoundary">The upper boundary.</param>
        /// <returns>Returns the series in range.</returns>
        public static ulong[] GetSeriesInRange(ulong lowerBoundary, ulong upperBoundary)
        {
            var fibonacciSeries = new List<ulong>
                                      {
                                          0,
                                          1
                                      };

            checked
            {
                try
                {
                    int i = 1;
                    do
                    {
                        ulong newVal = fibonacciSeries[i] + fibonacciSeries[i - 1];
                        if (newVal < upperBoundary)
                        {
                            fibonacciSeries.Add(newVal);
                        }
                        else
                        {
                            break;
                        }

                        i++;
                    }
                    while (true);
                }
                catch (OverflowException)
                {
                }
            }

            int startIndex = 0;

            for (int i = 0; i < fibonacciSeries.Count; i++)
            {
                if (fibonacciSeries[i] >= lowerBoundary)
                {
                    startIndex = i;
                    break;
                }
            }

            var seriesToOutput = new List<ulong>();
            for (int i = startIndex; i < fibonacciSeries.Count; i++)
            {
                seriesToOutput.Add(fibonacciSeries[i]);
            }

            return seriesToOutput.ToArray();
        }
    }
}
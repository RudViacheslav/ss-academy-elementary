﻿// <copyright file="Program.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task8
{
    using System;

    using InputHandling;

    // Программа позволяет вывести все числа Фибоначчи, которые находятся в указанном диапазоне.
    // Диапазон задаётся двумя аргументами при вызове главного класса.
    // Числа выводятся через запятую по возрастанию.

    /// <summary>
    ///     Contains Main.
    /// </summary>
    public class Program
    {
        /// <summary>
        ///     Defines the entry point of the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public static void Main(string[] args)
        {
            InputHandlerYesNo ynHandler = new InputHandlerYesNo("Enter another value?");
            InputHandlerUlong valueHandler = new InputHandlerUlong("Type value:");

            do
            {
                valueHandler.InvitationText = "Type lower bound:";
                ulong lower = valueHandler.GetInput();

                valueHandler.InvitationText = "Type upper bound";
                ulong upper = valueHandler.GetInput();

                var series = FibonacciSeries.GetSeriesInRange(lower, upper);
                Console.WriteLine();

                foreach (ulong item in series)
                {
                    Console.WriteLine(item);
                }
            }
            while (ynHandler.GetInput());

            // var series = FibonacciSeries.GetSeriesInRange(0, ulong.MaxValue);
            // foreach (ulong item in series)
            // {
            // Console.WriteLine(item);
            // }

            // Console.ReadLine();
        }
    }
}
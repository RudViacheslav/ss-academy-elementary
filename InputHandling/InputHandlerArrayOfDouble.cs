﻿// <copyright file="InputHandlerArrayOfDouble.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace InputHandling
{
    /// <summary>
    ///     Handle input to fill array of double values.
    /// </summary>
    /// <seealso cref="InputHandling.InputHandlerNumeric" />
    public class InputHandlerArrayOfDouble : InputHandlerNumeric
    {
        /// <summary>
        ///     The handler double
        /// </summary>
        private readonly InputHandlerDouble handlerDouble;

        /// <summary>
        ///     Initializes a new instance of the <see cref="InputHandlerArrayOfDouble" /> class.
        /// </summary>
        /// <param name="invitationText">Text that will be written to console when input required.</param>
        public InputHandlerArrayOfDouble(string invitationText)
            : base(invitationText)
        {
            handlerDouble = new InputHandlerDouble(invitationText);
        }

        /// <summary>
        ///     Gets the input.
        /// </summary>
        /// <param name="size">The size.</param>
        /// <returns>Returns array of double.</returns>
        public double[] GetInput(int size)
        {
            var array = new double[size];

            for (int i = 0; i < size; i++)
            {
                handlerDouble.InvitationText = "Side" + (i + 1);
                double newValue = handlerDouble.GetInput();
                array[i] = newValue;
            }

            return array;
        }
    }
}
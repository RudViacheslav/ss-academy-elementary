﻿// <copyright file="InputHandlerDouble.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace InputHandling
{
    using System;

    /// <summary>
    ///     Handle input from console and convert it to Double
    /// </summary>
    /// <seealso cref="InputHandling.InputHandlerNumeric" />
    public class InputHandlerDouble : InputHandlerNumeric
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="InputHandlerDouble" /> class.
        /// </summary>
        /// <param name="invitationText">Text that will be written to console when input required.</param>
        public InputHandlerDouble(string invitationText)
            : base(invitationText)
        {
        }

        /// <summary>
        ///     Request user for input until user enters right value.
        /// </summary>
        /// <returns>User entered value converted to double.</returns>
        public double GetInput()
        {
            while (true)
            {
                Console.WriteLine(InvitationText);
                string input = Console.ReadLine();
                input = input.Replace(',', '.');
                double value = 0;

                try
                {
                    value = Convert.ToDouble(input);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Wrong input.\n{0}\nTry again.", e.Message);
                    continue;
                }

                // if value negative, when it can't be, or if it less than MinimumValue
                if (value < MinimumValue)
                {
                    Console.WriteLine("Value less than minimum value({0}).\nTry again.", MinimumValue);
                    continue;
                }

                if (!CanBeNegative && value < 0)
                {
                    Console.WriteLine("Value can't be negative.\nTryAgain.");
                    continue;
                }

                return value;
            }
        }
    }
}
﻿// <copyright file="InputHandlerTextFromFile.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace InputHandling
{
    using System;
    using System.IO;

    /// <summary>
    ///     Handles input to read file.
    /// </summary>
    /// <seealso cref="InputHandling.InputHandler" />
    public class InputHandlerTextFromFile : InputHandler
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="InputHandlerTextFromFile" /> class.
        /// </summary>
        /// <param name="invitationText">Text that will be written to console when input required.</param>
        public InputHandlerTextFromFile(string invitationText)
            : base(invitationText)
        {
        }

        /// <summary>
        ///     Handles input with path to file, read this file and returns content of this file as text.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns>
        ///     Returns content of file as string.
        /// </returns>
        public string GetInput(out string path)
        {
            while (true)
            {
                Console.WriteLine(InvitationText);

                path = Console.ReadLine();

                path = path.Replace("\"", string.Empty);

                if (!File.Exists(path))
                {
                    Console.WriteLine("File not exist. Try again.");
                }
                else
                {
                    return File.ReadAllText(path);
                }
            }
        }
    }
}
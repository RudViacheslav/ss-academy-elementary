﻿// <copyright file="InputHandlerNumeric.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace InputHandling
{
    /// <summary>
    ///     Provide properites CanBeNegative and MinimumValue.
    /// </summary>
    public abstract class InputHandlerNumeric : InputHandler
    {
        public InputHandlerNumeric(string invitationText)
            : base(invitationText)
        {
        }

        /// <summary>
        ///     Gets or sets a value indicating whether this instance can be negative.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance can be negative; otherwise, <c>false</c>.
        /// </value>
        public bool CanBeNegative { get; set; } = false;

        public int MaximumValue { get; set; }

        public int MinimumValue { get; set; }
    }
}
﻿// <copyright file="InputHandlerUlong.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace InputHandling
{
    using System;

    /// <summary>
    ///     Handle input from console and convert it int.
    /// </summary>
    /// <seealso cref="InputHandling.InputHandlerNumeric" />
    public class InputHandlerUlong : InputHandlerNumeric
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="InputHandlerUlong" /> class.
        /// </summary>
        /// <param name="invitationText">The invitation text.</param>
        public InputHandlerUlong(string invitationText)
            : base(invitationText)
        {
        }

        /// <summary>
        ///     Gets or sets the maximum value.
        /// </summary>
        /// <value>
        ///     The maximum value.
        /// </value>
        public new ulong MaximumValue { get; set; }

        /// <summary>
        ///     Gets or sets the minimum value.
        /// </summary>
        /// <value>
        ///     The minimum value.
        /// </value>
        public new ulong MinimumValue { get; set; }

        /// <summary>
        ///     Gets the input from console and return it.
        /// </summary>
        /// <returns>Returns input value</returns>
        public ulong GetInput()
        {
            while (true)
            {
                Console.WriteLine(InvitationText);
                string input = Console.ReadLine();
                ulong value = 0;

                try
                {
                    value = Convert.ToUInt64(input);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Wrong input.\n{0}\nTry again.", e.Message);
                    continue;
                }

                if (value < MinimumValue)
                {
                    Console.WriteLine("Value less than minimum value({0}).\nTry again.", MinimumValue);
                    continue;
                }

                return value;
            }
        }
    }
}
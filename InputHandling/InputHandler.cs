﻿// <copyright file="InputHandler.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace InputHandling
{
    /// <summary>
    ///     Provide InvitationText field
    /// </summary>
    public abstract class InputHandler
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="InputHandler" /> class.
        /// </summary>
        /// <param name="invitationText">Text that will be written to console when input required.</param>
        public InputHandler(string invitationText)
        {
            InvitationText = invitationText;
        }

        /// <summary>
        ///     Gets or sets text that will be written to console when input required.
        /// </summary>
        public string InvitationText { get; set; }
    }
}
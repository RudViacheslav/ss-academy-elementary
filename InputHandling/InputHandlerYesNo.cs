﻿// <copyright file="InputHandlerYesNo.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace InputHandling
{
    using System;

    /// <summary>
    ///     Handle input form console and convert it to true or false.
    /// </summary>
    /// <seealso cref="InputHandling.InputHandler" />
    public class InputHandlerYesNo : InputHandler
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="InputHandlerYesNo" /> class.
        /// </summary>
        /// <param name="invitationText">Text that will be written to console when input required.</param>
        public InputHandlerYesNo(string invitationText)
            : base(invitationText)
        {
        }

        /// <summary>
        ///     Gets the input from console.
        /// </summary>
        /// <returns>Returns true if user entered y or yes, else return false</returns>
        public bool GetInput()
        {
            Console.WriteLine(InvitationText);
            string input = Console.ReadLine();

            string formatted = input.ToLower();

            if (formatted == "y" ||
                formatted == "yes")
            {
                return true;
            }

            return false;
        }
    }
}
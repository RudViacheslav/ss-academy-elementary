﻿// <copyright file="InputHandlerInt.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace InputHandling
{
    using System;

    /// <summary>
    ///     Handle input from console and convert it int.
    /// </summary>
    /// <seealso cref="InputHandling.InputHandlerNumeric" />
    public class InputHandlerInt : InputHandlerNumeric
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="InputHandlerInt" /> class.
        /// </summary>
        /// <param name="invitationText">The invitation text.</param>
        public InputHandlerInt(string invitationText)
            : base(invitationText)
        {
        }

        /// <summary>
        ///     Gets the input from console and return it.
        /// </summary>
        /// <returns>Returns input value</returns>
        public int GetInput()
        {
            while (true)
            {
                Console.WriteLine(InvitationText);
                string input = Console.ReadLine();
                int value = 0;

                try
                {
                    value = Convert.ToInt32(input);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Wrong input.\n{0}\nTry again.", e.Message);
                    continue;
                }

                // if value negative, when it can't be, or if it less than MinimumValue
                if (!CanBeNegative && value < 0)
                {
                    Console.WriteLine("Value can't be negative.\nTryAgain.");
                    continue;
                }

                if (value < MinimumValue)
                {
                    Console.WriteLine("Value less than minimum value({0}).\nTry again.", MinimumValue);
                    continue;
                }

                return value;
            }
        }
    }
}
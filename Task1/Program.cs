﻿// <copyright file="Program.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task1
{
    // Вывести шахматную доску с заданными размерами высоты и ширины, по принципу:
    // ******
    // ******
    // ******
    // ******
    // Программа запускается через вызов главного класса с параметрами.
    using System;

    using InputHandling;

    /// <summary>
    ///     Contain Main.
    /// </summary>
    public class Program
    {
        /// <summary>
        ///     Defines the entry point of the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public static void Main(string[] args)
        {
            InputHandlerYesNo handlerYN = new InputHandlerYesNo("Do you want continue?");
            do
            {
                InputHandlerInt inputHandler = new InputHandlerInt("Width:")
                                                   {
                                                       MinimumValue = 2,
                                                       CanBeNegative = false
                                                   };

                int width = inputHandler.GetInput();
                inputHandler.InvitationText = "Height:";
                int height = inputHandler.GetInput();

                Chessboard chessboard = new Chessboard(height, width);

                try
                {
                    WriteChar2dArray(chessboard.GetCharChessboard(), width, height);
                }
                catch (OutOfMemoryException)
                {
                    Console.WriteLine("Not enough memory, please use smaller values");
                }
            }
            while (!handlerYN.GetInput());
        }

        /// <summary>
        ///     Writes the char2d array to console.
        /// </summary>
        /// <param name="chessboard">The chessboard.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        private static void WriteChar2dArray(char[,] chessboard, int width, int height)
        {
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    Console.Write(chessboard[y, x]);
                }

                Console.WriteLine();
            }
        }
    }
}
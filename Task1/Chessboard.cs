﻿// <copyright file="Chessboard.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task1
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    ///     Represent chessboard with width and height in cells.
    /// </summary>
    public class Chessboard
    {
        /// <summary>
        ///     The height of cessboard
        /// </summary>
        private readonly int height;

        /// <summary>
        ///     The width of chessboard
        /// </summary>
        private readonly int width;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Chessboard" /> class.
        /// </summary>
        /// <param name="height">Height of chessboard</param>
        /// <param name="width">Width of chessboard</param>
        /// <exception cref="ArgumentException">Height and width must bee more than 1</exception>
        public Chessboard(int height, int width)
        {
            if (height < 2 || width < 2)
            {
                throw new ArgumentException("Height and width must bee more than 1");
            }

            this.height = height;
            this.width = width;
        }

        /// <summary>
        ///     Types of Cell
        /// </summary>
        private enum Cell
        {
            /// <summary>
            ///     The white cell
            /// </summary>
            White = 0,

            /// <summary>
            ///     The black cell
            /// </summary>
            Black = 1
        }

        /// <summary>
        ///     Generates and returns 2d char array filled white and black symbols.
        /// </summary>
        /// <param name="whtieSymbol">Char that will represent white cell in chessboard.</param>
        /// <param name="blackSymbol">Char that will represent black cell in chessboard.</param>
        /// <returns>Returns 2d char array filled white and black symbols.</returns>
        public char[,] GetCharChessboard(char whtieSymbol = ' ', char blackSymbol = '#')
        {
            var placeholders = new Dictionary<Cell, char>(2)
                                   {
                                       [Cell.White] = whtieSymbol,
                                       [Cell.Black] = blackSymbol
                                   };

            var chessboard = new char[height, width];
            Cell current = Cell.White;

            bool isEven = width % 2 == 0;
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    chessboard[y, x] = placeholders[current];

                    // invert cell
                    if (current == Cell.White)
                    {
                        current = Cell.Black;
                    }
                    else
                    {
                        current = Cell.White;
                    }
                }

                // invert cell
                if (isEven)
                {
                    // invert cell on new line. Last and first cell of two strings must be same.
                    if (current == Cell.White)
                    {
                        current = Cell.Black;
                    }
                    else
                    {
                        current = Cell.White;
                    }
                }
            }

            return chessboard;
        }

        /// <summary>
        ///     Generates and returns 2d char array filled white and black symbols.
        /// </summary>
        /// <param name="whtieSymbol">Char that will represent white cell in chessboard.</param>
        /// <param name="blackSymbol">Char that will represent black cell in chessboard.</param>
        /// <returns>Returns 2d char array filled white and black symbols</returns>
        public char[,] GetCharChessboardIfless(char whtieSymbol = ' ', char blackSymbol = '#')
        {
            // clearer and faster(~1.9 times) than many if's
            var placeholdersBool = new Dictionary<bool, char>(2)
                                       {
                                           [false] = whtieSymbol,
                                           [true] = blackSymbol
                                       };

            var chessboard = new char[height, width];
            bool currentCell = false;

            bool isEven = width % 2 == 0;

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    chessboard[y, x] = placeholdersBool[currentCell];

                    // invert cell
                    currentCell = !currentCell;
                }

                // invert cell on new line. Last and first cell of two strings must be same.
                if (isEven)
                {
                    currentCell = !currentCell;
                }
            }

            return chessboard;
        }
    }
}
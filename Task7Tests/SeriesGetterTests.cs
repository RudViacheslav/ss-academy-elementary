﻿// <copyright file="SeriesGetterTests.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task7.Tests
{
    using System;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class SeriesGetterTests
    {
        [TestMethod]
        public void GetSeriesOfNumbersSquareOfWhichLowerThanValueTest_432_20()
        {
            var a = SeriesGetter.GetSeriesOfNumbersSquareOfWhichLowerThanValue(432);
            Assert.AreEqual(20, a.Last());
        }

        [TestMethod]
        public void GetSeriesOfNumbersSquareOfWhichLowerThanValueTest_MaxValue_exception()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => SeriesGetter.GetSeriesOfNumbersSquareOfWhichLowerThanValue(int.MaxValue));
        }

        [TestMethod]
        public void GetSeriesOfNumbersSquareOfWhichLowerThanValueTest_negativeValue_exception()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => SeriesGetter.GetSeriesOfNumbersSquareOfWhichLowerThanValue(-4));
        }
    }
}
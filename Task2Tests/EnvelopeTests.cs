﻿// <copyright file="EnvelopeTests.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task2.Tests
{
    using System;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class EnvelopeTests
    {
        [TestMethod]
        public void CheckCanContainTest_3x4_3x4_true()
        {
            Envelope env1 = new Envelope(3, 4);
            Envelope env2 = new Envelope(3, 4);

            bool result = env1.CheckCanContain(env2) && env2.CheckCanContain(env1);

            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void CheckCanContainTest_3x4_3x5_false_true()
        {
            Envelope env1 = new Envelope(3, 4);
            Envelope env2 = new Envelope(3, 5);

            bool result = !env1.CheckCanContain(env2) && env2.CheckCanContain(env1);

            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void CheckCanContainTest_3x4_8x15_false_true()
        {
            Envelope env1 = new Envelope(10, 10);
            Envelope env2 = new Envelope(13, 15);

            bool result = !env1.CheckCanContain(env2) && env2.CheckCanContain(env1);

            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void CheckCanContainTest_5x5_1x6_true()
        {
            Envelope env1 = new Envelope(5, 5);
            Envelope env2 = new Envelope(1, 6);

            bool res1 = env1.CheckCanContain(env2);
            bool res2 = env2.CheckCanContain(env1);

            bool result = env1.CheckCanContain(env2) && !env2.CheckCanContain(env1);

            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void EnvelopeTestInitializeLowerThenMin_01_ThrowsException()
        {
            Assert.ThrowsException<ArgumentException>(() => new Envelope(-3, 0));
        }
    }
}
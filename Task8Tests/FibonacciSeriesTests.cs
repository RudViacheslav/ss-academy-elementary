﻿// <copyright file="FibonacciSeriesTests.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task8.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class FibonacciSeriesTests
    {
        [TestMethod]
        public void GetSeriesInRangeTest()
        {
            var series = FibonacciSeries.GetSeriesInRange(0, ulong.MaxValue);
            Assert.AreEqual(94, series.Length);
        }
    }
}
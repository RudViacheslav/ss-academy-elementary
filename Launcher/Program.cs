﻿namespace Launcher
{
    using InputHandling;

    internal class Program
    {
        private static void Main(string[] args)
        {
            InputHandlerYesNo ynHandler = new InputHandlerYesNo("Repeat?");
            InputHandlerInt intHandler = new InputHandlerInt("Type number of task you want to run")
                                             {
                                                 CanBeNegative = false,
                                                 MinimumValue = 1,
                                                 MaximumValue = 8
                                             };
            do
            {
                int task = intHandler.GetInput();

                switch (task)
                {
                    case 1:
                        Task1.Program.Main(null);
                        break;
                    case 2:
                        Task2.Program.Main(null);
                        break;
                    case 3:
                        Task3.Program.Main(null);
                        break;
                    case 4:
                        Task4.Program.Main(null);
                        break;
                    case 5:
                        Task5.Program.Main(null);
                        break;
                    case 6:
                        Task6.Program.Main(null);
                        break;
                    case 7:
                        Task6.Program.Main(null);
                        break;
                    case 8:
                        Task6.Program.Main(null);
                        break;

                    default:
                        break;
                }
            }
            while (ynHandler.GetInput());
        }
    }
}
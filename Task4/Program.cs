﻿// <copyright file="Program.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task4
{
    using System;
    using System.IO;

    using InputHandling;

    // Нужно написать программу, которая будет иметь два режима:
    // Считать количество вхождений строки в текстовом файле.
    // Делать замену строки на другую в указанном файле
    // Программа должна принимать аргументы на вход при запуске:
    // <путь к файлу> <строка для подсчёта>
    // <путь к файлу> <строка для поиска> <строка для замены>

    /// <summary>
    ///     Contains Main.
    /// </summary>
    public class Program
    {
        /// <summary>
        ///     Defines the entry point of the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public static void Main(string[] args)
        {
            InputHandlerYesNo handlerYesNo = new InputHandlerYesNo("Enter another one?");
            InputHandlerTextFromFile textHandler = new InputHandlerTextFromFile("Type path to file here:");

            do
            {
                string text = textHandler.GetInput(out string path);

                Console.WriteLine("Type text that you want to replace:");
                string match = Console.ReadLine();

                Console.WriteLine("Type thext that you want to isert instead:");
                string replace = Console.ReadLine();

                string newText = StringFindAndReplace.ReplaceKeepCapitalization(text, match, replace);

                File.WriteAllText(path, newText);
            }
            while (handlerYesNo.GetInput());
        }
    }
}
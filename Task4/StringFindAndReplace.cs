﻿// <copyright file="StringFindAndReplace.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task4
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    ///     Contain methods to find and replace substrings case-insensitive.
    /// </summary>
    public static class StringFindAndReplace
    {
        /// <summary>
        ///     Changes the capitalization of string by array of bool. True is Upper case.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="capitalization">The capitalization.</param>
        /// <returns>Returns new string with changed capitalization.</returns>
        public static string ChangeCapitalization(string source, bool[] capitalization)
        {
            StringBuilder text = new StringBuilder(source);
            int length = Math.Min(capitalization.Length, source.Length);

            for (int i = 0; i < length; i++)
            {
                if (capitalization[i])
                {
                    text[i] = char.ToUpper(text[i]);
                }
                else
                {
                    text[i] = char.ToLower(text[i]);
                }
            }

            return text.ToString();
        }

        /// <summary>
        ///     Gets all occurences in source.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="matchString">The match string.</param>
        /// <returns>
        ///     Returns indexes of occurences
        /// </returns>
        public static List<Occurrence> GetAllOccurrences(string source, string matchString)
        {
            string lowerCaseString = source.ToLower();
            var occurences = new List<Occurrence>();

            // passing text
            for (int i = 0; i < lowerCaseString.Length; i++)
            {
                // if any char same as first char of matchString
                if (lowerCaseString[i] == matchString[0])
                {
                    int startIndex = i;

                    if (lowerCaseString.Length >= i + matchString.Length)
                    {
                        // continue in new loop with new counter from start of matchString
                        for (int j = 0; j < matchString.Length; j++)
                        {
                            // check whether characters are same
                            if (!(lowerCaseString[i + j] == matchString[j]))
                            {
                                // characters not same. Break comparing loop.
                                i += j;
                                break;
                            }

                            // if it last character, all characters are same
                            if (j == matchString.Length - 1)
                            {
                                var newCap = GetCapitalization(source.Substring(startIndex, matchString.Length));
                                occurences.Add(new Occurrence(startIndex, i + j, newCap));
                                i += j;
                            }
                        }
                    }
                }
            }

            return occurences;
        }

        /// <summary>
        ///     Gets the capitalization of string.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns>Returns the bool array which contain capitalization for each symbol in string. True is upper case.</returns>
        public static bool[] GetCapitalization(string text)
        {
            var capitalization = new bool[text.Length];

            for (int i = 0; i < text.Length; i++)
            {
                capitalization[i] = char.IsUpper(text[i]);
            }

            return capitalization;
        }

        /// <summary>
        ///     Replaces substring in string with capitalization retention.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="matchString">The string.</param>
        /// <param name="replaceString">The string to replace.</param>
        /// <returns>Returns substring in string with capitalization retention.</returns>
        public static string ReplaceKeepCapitalization(string text, string matchString, string replaceString)
        {
            var occurrences = GetAllOccurrences(text, matchString.ToLower());
            StringBuilder textSB = new StringBuilder(text);

            // remove all occurences of matchString
            for (int i = occurrences.Count - 1; i >= 0; i--)
            {
                textSB.Remove(occurrences[i].Start, matchString.Length);
            }

            // where new string will be placed
            var newIndexes = new int[occurrences.Count];

            // find new indexes to place new string
            for (int i = 0; i < occurrences.Count; i++)
            {
                newIndexes[i] = occurrences[i].Start - matchString.Length * i;
            }

            // insert new string
            for (int i = newIndexes.Length - 1; i >= 0; i--)
            {
                string textToInsert = ChangeCapitalization(replaceString, occurrences[i].Capitalization);
                textSB.Insert(newIndexes[i], textToInsert);
            }

            return textSB.ToString();
        }
    }
}
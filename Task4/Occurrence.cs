﻿// <copyright file="Occurrence.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task4
{
    /// <summary>
    ///     Contain start and end indexes of occurence.
    /// </summary>
    public struct Occurrence
    {
        /// <summary>
        ///     Start index of occurence.
        /// </summary>
        public int Start;

        /// <summary>
        ///     The capitalization of substring.
        /// </summary>
        public bool[] Capitalization;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Occurrence" /> struct.
        /// </summary>
        /// <param name="start">The start.</param>
        /// <param name="end">The end.</param>
        /// <param name="capitalization">The capitalization.</param>
        public Occurrence(int start, int end, bool[] capitalization)
        {
            Start = start;
            Capitalization = capitalization;
        }
    }
}
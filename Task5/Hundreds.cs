﻿// <copyright file="Hundreds.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task5
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    ///     Hundreds part of number.
    /// </summary>
    /// <seealso cref="Task5.NumberPart" />
    internal class Hundreds : NumberPart
    {
        /// <summary>
        ///     The default dictionary
        /// </summary>
        private static readonly Dictionary<int, string> DefaultDictionary;

        /// <summary>
        ///     Initializes the <see cref="Hundreds" /> class.
        /// </summary>
        static Hundreds()
        {
            DefaultDictionary = new Dictionary<int, string>
                                    {
                                        { 1, "сто" },
                                        { 2, "сти" },
                                        { 3, "ста" },
                                        { 4, "ста" },
                                        { 5, "сот" },
                                        { 6, "сот" },
                                        { 7, "сот" },
                                        { 8, "сот" },
                                        { 9, "сот" }
                                    };
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Hundreds" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <exception cref="ArgumentNullException">Parent part of number.</exception>
        public Hundreds(NumberPart parent)
            : base(parent)
        {
            if (parent == null)
            {
                throw new ArgumentNullException(nameof(parent));
            }

            Gender = Gender.Female;

            Dictionary = DefaultDictionary;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Hundreds" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="dictionary">The dictionary.</param>
        /// <exception cref="ArgumentNullException">Parent part of number.</exception>
        public Hundreds(NumberPart parent, Dictionary<int, string> dictionary)
            : base(parent)
        {
            if (parent == null)
            {
                throw new ArgumentNullException(nameof(parent));
            }

            Gender = Gender.Female;

            Dictionary = dictionary;
        }

        /// <summary>
        ///     Gets or sets the dictionary.
        /// </summary>
        /// <value>
        ///     The dictionary.
        /// </value>
        public Dictionary<int, string> Dictionary { get; set; }

        /// <summary>
        ///     Gets the value.
        /// </summary>
        /// <value>
        ///     The value.
        /// </value>
        public override int Value => parent.Value;

        /// <summary>
        ///     Gets the number in word.
        /// </summary>
        /// <returns>Returns the number in word.</returns>
        public override string GetString()
        {
            string parentPart = string.Empty;
            Ones one = parent as Ones;
            string valueToReturn = string.Empty;

            parentPart = one.Parent?.GetString();

            // 100
            if (Value == 1)
            {
                valueToReturn = parentPart + Dictionary[1];
            }

            // 200 300 400 ...
            else if (Value != 0)
            {
                parentPart = one.GetString(Gender);
                valueToReturn = parentPart + Dictionary[one.Value];
            }
            else
            {
                valueToReturn = parentPart;
            }

            return valueToReturn + " ";
        }
    }
}
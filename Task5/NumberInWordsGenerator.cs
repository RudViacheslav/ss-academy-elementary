﻿// <copyright file="NumberInWordsGenerator.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task5
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using Newtonsoft.Json;

    /// <summary>
    ///     Generates number in words.
    /// </summary>
    public class NumberInWordsGenerator
    {
        /// <summary>
        ///     The first part of path to dictionaries.
        /// </summary>
        private static readonly string DictionariesPath = @"Dictionaries" + Path.DirectorySeparatorChar;

        /// <summary>
        ///     The types of each digit in word, from lower digit to
        /// </summary>
        private static readonly DigitType[] DigitTypes =
            { DigitType.Ones, DigitType.Tens, DigitType.Hundreds, DigitType.Thousands, DigitType.Tens, DigitType.Hundreds };

        /// <summary>
        ///     The hundreds dictionary
        /// </summary>
        private Dictionary<int, string> hundredsDictionary;

        /// <summary>
        ///     The ones dictionary
        /// </summary>
        private Dictionary<int, GenderedWord> onesDictionary;

        /// <summary>
        ///     The tens dictionary
        /// </summary>
        private Dictionary<int, string> tensDictionary;

        /// <summary>
        ///     The tens teen dictionary
        /// </summary>
        private Dictionary<int, string> tensTeenDictionary;

        /// <summary>
        ///     The thousands dictionary
        /// </summary>
        private Dictionary<int, string> thousandsDictionary;

        /// <summary>
        ///     Initializes a new instance of the <see cref="NumberInWordsGenerator" /> class.
        /// </summary>
        public NumberInWordsGenerator()
        {
            // In future it is possible to add loading custom dictinaries by json
            InitializeDictionaries();
        }

        public const int MaxValue = 999_999;

        /// <summary>
        ///     Generates the number in word.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Returns string with the number in word.</returns>
        public string GenerateString(int value)
        {
            bool minus = false;
            if (value < 0)
            {
                minus = true;
                value = value * -1;
            }

            if (value > MaxValue)
            {
                throw new ArgumentOutOfRangeException();
            }

            var stack = GetStackOfDigitWithTypes(value);
            NumberPart numberPartOld = null;
            while (stack.Count > 0)
            {
                NumberPart numberPartNew = null;
                DigitTypePair stackItem = stack.Pop();
                int digit = stackItem.Digit;
                DigitType type = stackItem.Type;
                switch (type)
                {
                    case DigitType.Ones:
                        numberPartNew = new Ones(digit, onesDictionary, numberPartOld);
                        break;

                    case DigitType.Tens:
                        if (digit == 1)
                        {
                            numberPartNew = new Tens(
                                new Ones(digit, onesDictionary, numberPartOld),
                                tensDictionary,
                                tensTeenDictionary,
                                stack.Peek().Digit);
                        }
                        else
                        {
                            numberPartNew = new Tens(
                                new Ones(digit, onesDictionary, numberPartOld),
                                tensDictionary,
                                tensTeenDictionary);
                        }

                        break;

                    case DigitType.Hundreds:
                        numberPartNew = new Hundreds(
                            new Ones(digit, onesDictionary, numberPartOld),
                            hundredsDictionary);

                        break;

                    case DigitType.Thousands:
                        numberPartNew = new Thousands(
                            new Ones(digit, onesDictionary, numberPartOld),
                            thousandsDictionary);

                        break;

                    default:
                        break;
                }

                numberPartOld = numberPartNew;
            }

            string stringToReturn = numberPartOld.GetString().Replace("   ", " ");
            stringToReturn = stringToReturn.Replace("  ", " ");

            if (minus)
            {
                return "минус " + stringToReturn;
            }

            return stringToReturn;
        }

        /// <summary>
        ///     Gets the stack of digit with types.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Returns the stack of digit with types.</returns>
        private static Stack<DigitTypePair> GetStackOfDigitWithTypes(int value)
        {
            var stack = new Stack<DigitTypePair>();
            int i = 0;

            do
            {
                stack.Push(new DigitTypePair { Digit = value % 10, Type = DigitTypes[i] });
                value /= 10;
                i++;
            }
            while (value > 0);

            return stack;
        }

        /// <summary>
        ///     Initializes the d ictionaries.
        /// </summary>
        private void InitializeDictionaries()
        {
            T LoadDictionary<T>(string fileName)
            {
                string path = DictionariesPath + fileName;
                string json = File.ReadAllText(path);
                return JsonConvert.DeserializeObject<T>(json);
            }

            if (File.Exists(DictionariesPath + @"Ones.json"))
            {
                onesDictionary = LoadDictionary<Dictionary<int, GenderedWord>>(@"Ones.json");
            }

            if (File.Exists(DictionariesPath + @"TensTeen.json"))
            {
                tensTeenDictionary = LoadDictionary<Dictionary<int, string>>(@"TensTeen.json");
            }

            if (File.Exists(DictionariesPath + @"Tens.json"))
            {
                tensDictionary = LoadDictionary<Dictionary<int, string>>(@"Tens.json");
            }

            if (File.Exists(DictionariesPath + @"Hundreds.json"))
            {
                hundredsDictionary = LoadDictionary<Dictionary<int, string>>(@"Hundreds.json");
            }

            if (File.Exists(DictionariesPath + @"Thousands.json"))
            {
                thousandsDictionary = LoadDictionary<Dictionary<int, string>>(@"Thousands.json");
            }
        }
    }
}
﻿// <copyright file="Tens.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task5
{
    using System.Collections.Generic;

    /// <summary>
    ///     Tens part.
    /// </summary>
    /// <seealso cref="Task5.NumberPart" />
    internal class Tens : NumberPart
    {
        /// <summary>
        ///     The default dictionary
        /// </summary>
        private static readonly Dictionary<int, string> DefaultDictionary;

        /// <summary>
        ///     The default teen dictionary
        /// </summary>
        private static readonly Dictionary<int, string> DefaultTeenDictionary;

        /// <summary>
        ///     The child value.
        /// </summary>
        private readonly int childValue;

        /// <summary>
        ///     Initializes the <see cref="Tens" /> class.
        /// </summary>
        static Tens()
        {
            DefaultDictionary = new Dictionary<int, string>
                                    {
                                        { 0, string.Empty },
                                        { 1, "десять" },
                                        { 2, "двадцать" },
                                        { 3, "тридцать" },
                                        { 4, "сорок" },
                                        { 5, "пятдесят" },
                                        { 6, "шестьдесят" },
                                        { 7, "семьдесят" },
                                        { 8, "восемьдесят" },
                                        { 9, "девяносто" }
                                    };

            DefaultTeenDictionary = new Dictionary<int, string>
                                        {
                                            { 0, "десять" },
                                            { 1, "одиннадцать" },
                                            { 2, "двенадцать" },
                                            { 3, "тринадцать" },
                                            { 4, "четырнадцать" },
                                            { 5, "пятнадцвать" },
                                            { 6, "шестнадцать" },
                                            { 7, "семнадцать" },
                                            { 8, "восемнадцать" },
                                            { 9, "девятнадцать" }
                                        };
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Tens" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="childValue">The child value.</param>
        public Tens(NumberPart parent, int childValue = 0)
            : base(parent)
        {
            Dictionary = DefaultDictionary;

            TeenDictionary = DefaultTeenDictionary;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Tens" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="dictionary">The dictionary.</param>
        /// <param name="teenDictionary">The teen dictionary.</param>
        /// <param name="childValue">The child value.</param>
        public Tens(NumberPart parent, Dictionary<int, string> dictionary, Dictionary<int, string> teenDictionary, int childValue = 0)
            : base(parent)
        {
            Dictionary = dictionary;

            TeenDictionary = teenDictionary;
        }

        /// <summary>
        ///     Gets or sets the dictionary.
        /// </summary>
        /// <value>
        ///     The dictionary.
        /// </value>
        public Dictionary<int, string> Dictionary { get; set; }

        /// <summary>
        ///     Gets or sets the gender.
        /// </summary>
        /// <value>
        ///     The gender.
        /// </value>
        public override Gender Gender => parent.Gender;

        /// <summary>
        ///     Gets or sets the words for 11 12 13 14 15 16 17 18 19
        /// </summary>
        /// <value>
        ///     The teen dictionary.
        /// </value>
        public Dictionary<int, string> TeenDictionary { get; set; }

        /// <summary>
        ///     Gets the value.
        /// </summary>
        /// <value>
        ///     The value.
        /// </value>
        public override int Value => parent.Value;

        /// <summary>
        ///     Gets the number in words.
        /// </summary>
        /// <returns>
        ///     Returns the number in words.
        /// </returns>
        public override string GetString()
        {
            string firstPart = string.Empty;

            if (parent.Parent != null)
            {
                firstPart = parent.Parent.GetString();
            }

            // without last digit, like 10, 11, 20
            if (childValue != 0)
            {
                // do not add next digit but add all string before

                // if 10 11 12 13 14 ...
                if (Value == 1)
                {
                    return firstPart + TeenDictionary[childValue] + " ";
                }

                // 20 30 40 50
                return firstPart + Dictionary[Value] + " ";
            }

            // if ten digit is 0
            if (Value == 0)
            {
                return firstPart + " ";
            }

            // 25 84 43 ...
            return firstPart + Dictionary[Value] + " ";
        }
    }
}
﻿// <copyright file="Ones.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task5
{
    using System.Collections.Generic;

    using Newtonsoft.Json;

    /// <summary>
    ///     Ones part.
    /// </summary>
    /// <seealso cref="Task5.NumberPart" />
    internal class Ones : NumberPart
    {
        /// <summary>
        ///     The default dictionary
        /// </summary>
        private static readonly Dictionary<int, GenderedWord> DefaultDictionary;

        /// <summary>
        ///     The default gender
        /// </summary>
        private readonly Gender defaultGender = Gender.Male;

        /// <summary>
        ///     The value
        /// </summary>
        private readonly int value;

        /// <summary>
        ///     Initializes the <see cref="Ones" /> class.
        /// </summary>
        static Ones()
        {
            DefaultDictionary = new Dictionary<int, GenderedWord>
                                    {
                                        { 0, new GenderedWord("ноль", "ноль") },
                                        { 1, new GenderedWord("один", "одна") },
                                        { 2, new GenderedWord("два", "две") },
                                        { 3, new GenderedWord("три", "три") },
                                        { 4, new GenderedWord("четыре", "четыре") },
                                        { 5, new GenderedWord("пять", "пять") },
                                        { 6, new GenderedWord("шесть", "шесть") },
                                        { 7, new GenderedWord("семь", "семь") },
                                        { 8, new GenderedWord("восемь", "восемь") },
                                        { 9, new GenderedWord("девять", "девять") }
                                    };
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Ones" /> class.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="parent">The parent.</param>
        public Ones(int value, NumberPart parent = null)
            : base(parent)
        {
            this.value = value;
            Dictionary = DefaultDictionary;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Ones" /> class.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="dictionary">The dictionary.</param>
        /// <param name="parent">The parent.</param>
        public Ones(int value, Dictionary<int, GenderedWord> dictionary, NumberPart parent = null)
            : base(parent)
        {
            this.value = value;
            Dictionary = dictionary;
        }

        /// <summary>
        ///     Gets or sets the dictionary.
        /// </summary>
        /// <value>
        ///     The dictionary.
        /// </value>
        public Dictionary<int, GenderedWord> Dictionary { get; set; }

        /// <summary>
        ///     Gets the value.
        /// </summary>
        /// <value>
        ///     The value.
        /// </value>
        public override int Value => value;

        /// <summary>
        ///     Gets the number in words.
        /// </summary>
        /// <returns>
        ///     Returns the number in words.
        /// </returns>
        public override string GetString()
        {
            // if not single digit like 0 1 2 3
            if (parent != null)
            {
                string firstPart = parent.GetString();

                // 11 12 13 14 15 ... Do not use thisPart to prevent seventeen seven
                if (parent is Tens tens &&
                    tens.Parent != null &&
                    value != 0 &&
                    parent.Value == 1)
                {
                    return firstPart;
                }

                // 10 20 30
                if (value == 0)
                {
                    return firstPart;
                }

                // 25 25
                string thisPart = Dictionary[value].GetWord(parent.Gender);
                return firstPart + " " + thisPart;
            }

            return Dictionary[value].GetWord(defaultGender);
        }

        /// <summary>
        ///     Gets the string.
        /// </summary>
        /// <param name="gender">The gender.</param>
        /// <returns>Returns the the string.</returns>
        public string GetString(Gender gender)
        {
            string firstPart = string.Empty;
            if (parent != null)
            {
                firstPart = parent?.GetString();
            }

            if (value != 0 && Parent?.Value != 1)
            {
                return firstPart + Dictionary[value].GetWord(gender);
            }

            return firstPart;
        }

        /// <summary>
        ///     Initializes the dictionary.
        /// </summary>
        /// <param name="jsonString">The json string.</param>
        public void InitializeDictionary(string jsonString)
        {
            Dictionary = JsonConvert.DeserializeObject<Dictionary<int, GenderedWord>>(jsonString);
        }
    }
}
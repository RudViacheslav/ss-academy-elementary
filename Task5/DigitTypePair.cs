﻿// <copyright file="DigitTypePair.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task5
{
    /// <summary>
    ///     Pair of Digit and it's type.
    /// </summary>
    internal struct DigitTypePair
    {
        /// <summary>
        ///     The digit.
        /// </summary>
        public int Digit;

        /// <summary>
        ///     The type.
        /// </summary>
        public DigitType Type;
    }
}
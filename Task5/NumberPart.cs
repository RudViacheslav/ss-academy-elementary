﻿// <copyright file="NumberPart.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task5
{
    /// <summary>
    ///     Represent single digit of number, or all number if digit only one.
    /// </summary>
    internal abstract class NumberPart
    {
        /// <summary>
        ///     Previous part of digtit.
        /// </summary>
        protected NumberPart parent;

        /// <summary>
        ///     Initializes a new instance of the <see cref="NumberPart" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public NumberPart(NumberPart parent = null)
        {
            this.parent = parent;
        }

        /// <summary>
        ///     Gets or sets the gender.
        /// </summary>
        /// <value>
        ///     The gender.
        /// </value>
        public virtual Gender Gender { get; protected set; }

        /// <summary>
        ///     Gets the parent.
        /// </summary>
        /// <value>
        ///     The parent.
        /// </value>
        public NumberPart Parent => parent;

        /// <summary>
        ///     Gets the value.
        /// </summary>
        /// <value>
        ///     The value.
        /// </value>
        public abstract int Value { get; }

        /// <summary>
        ///     Gets the number in words.
        /// </summary>
        /// <returns>Returns the number in words.</returns>
        public abstract string GetString();
    }
}
﻿// <copyright file="Thousands.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task5
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    ///     Thousands part of number.
    /// </summary>
    /// <seealso cref="Task5.NumberPart" />
    internal class Thousands : NumberPart
    {
        private static readonly Dictionary<int, string> DefaultDictionary;

        static Thousands()
        {
            DefaultDictionary = new Dictionary<int, string>
                                    {
                                        { 0, "тысяч" },
                                        { 1, "тысяча" },
                                        { 2, "тысячи" },
                                        { 3, "тысячи" },
                                        { 4, "тысячи" },
                                        { 5, "тысяч" },
                                        { 6, "тысяч" },
                                        { 7, "тысяч" },
                                        { 8, "тысяч" },
                                        { 9, "тысяч" }
                                    };
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Thousands" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <exception cref="ArgumentNullException">parent</exception>
        public Thousands(NumberPart parent)
            : base(parent)
        {
            if (this.parent == null)
            {
                throw new ArgumentNullException(nameof(this.parent));
            }

            Gender = Gender.Female;

            Dictionary = DefaultDictionary;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Thousands" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="dictionary">The dictionary.</param>
        /// <exception cref="ArgumentNullException">parent</exception>
        public Thousands(NumberPart parent, Dictionary<int, string> dictionary)
            : base(parent)
        {
            if (this.parent == null)
            {
                throw new ArgumentNullException(nameof(this.parent));
            }

            Gender = Gender.Female;

            Dictionary = dictionary;
        }

        /// <summary>
        ///     Gets or sets the dictionary.
        /// </summary>
        /// <value>
        ///     The dictionary.
        /// </value>
        public Dictionary<int, string> Dictionary { get; set; }

        /// <summary>
        ///     Gets the value.
        /// </summary>
        /// <value>
        ///     The value.
        /// </value>
        public override int Value => parent.Value;

        /// <summary>
        ///     Gets the number in words.
        /// </summary>
        /// <returns>
        ///     Returns the number in words.
        /// </returns>
        public override string GetString()
        {
            string parentString = ((Ones)parent).GetString(Gender);

            // if parent of parent is tens.
            if (Parent.Parent is Tens)
            {
                // 10 11 12 13 ... одиннадцать тысяч
                if (Parent.Parent.Value == 1)
                {
                    return parentString + " " + Dictionary[5] + " ";
                }
            }

            return parentString + " " + Dictionary[parent.Value] + " ";
        }
    }
}
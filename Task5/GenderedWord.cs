﻿// <copyright file="GenderedWord.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task5
{
    using System;

    using Newtonsoft.Json;

    /// <summary>
    ///     Contains two variants of word with different genders.
    /// </summary>
    [JsonObject]
    internal class GenderedWord
    {
        /// <summary>
        ///     The female word.
        /// </summary>
        [JsonProperty]
        private readonly string female;

        /// <summary>
        ///     The male word.
        /// </summary>
        [JsonProperty]
        private readonly string male;

        /// <summary>
        ///     Initializes a new instance of the <see cref="GenderedWord" /> class.
        /// </summary>
        /// <param name="male">The male word.</param>
        /// <param name="female">The female word.</param>
        public GenderedWord(string male, string female)
        {
            this.male = male;
            this.female = female;
        }

        /// <summary>
        ///     Gets the word.
        /// </summary>
        /// <param name="gender">The gender.</param>
        /// <returns>Returns the string with selected gender.</returns>
        /// <exception cref="ArgumentException">If gender not male or female.</exception>
        public string GetWord(Gender gender)
        {
            switch (gender)
            {
                case Gender.Male:
                    return male;

                case Gender.Female:
                    return female;

                default:
                    throw new ArgumentException();
            }
        }
    }
}
﻿// <copyright file="Program.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task5
{
    using System;
    using System.Text;
    using System.Diagnostics;

    using InputHandling;

    // Нужно преобразовать целое число в прописной вариант: 12 – двенадцать.
    // Программа запускается через вызов главного класса с параметрами.

    /// <summary>
    ///     Contains Main.
    /// </summary>
    public class Program
    {
        /// <summary>
        ///     Defines the entry point of the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            //InputHandlerYesNo HandlerYesNo = new InputHandlerYesNo("Enter another value?");
            //InputHandlerInt intHandler = new InputHandlerInt("Type value to convert to text")
            //                                 {
            //                                     MaximumValue = NumberInWordsGenerator.MaxValue,
            //                                     CanBeNegative = true
            //                                 };

            //do
            //{
            //    int value = intHandler.GetInput();
            //    string str = new NumberInWordsGenerator().GenerateString(value);
            //    Console.WriteLine(str);
            //}
            //while (HandlerYesNo.GetInput());
            var timer = new Stopwatch();
            while (true)
            {
                timer.Restart();
                var generator = new NumberInWordsGenerator();
                for (int i = 0; i < NumberInWordsGenerator.MaxValue; i++)
                {
                    generator.GenerateString(i);
                }

                timer.Stop();
                Console.WriteLine(timer.ElapsedMilliseconds);
                Console.ReadLine();
            }

        }
    }
}
﻿// <copyright file="DigitType.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task5
{
    /// <summary>
    ///     Represent type of digit in number.
    /// </summary>
    internal enum DigitType
    {
        /// <summary>
        ///     The ones
        /// </summary>
        Ones,

        /// <summary>
        ///     The tens
        /// </summary>
        Tens,

        /// <summary>
        ///     The hundreds
        /// </summary>
        Hundreds,

        /// <summary>
        ///     The thousands
        /// </summary>
        Thousands
    }
}
﻿// <copyright file="Gender.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task5
{
    /// <summary>
    ///     Gender of word.
    /// </summary>
    public enum Gender
    {
        /// <summary>
        ///     The male.
        /// </summary>
        Male,

        /// <summary>
        ///     The female.
        /// </summary>
        Female
    }
}
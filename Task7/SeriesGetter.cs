﻿// <copyright file="SeriesGetter.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task7
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    ///     Contain mathot which returns series of numbers square of which lower than value.
    /// </summary>
    public class SeriesGetter
    {
        /// <summary>
        ///     Gets the series of numbers square of which lower than value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Returns the series of numbers square of which lower than value.</returns>
        public static IEnumerable<ulong> GetSeriesOfNumbersSquareOfWhichLowerThanValue(ulong value)
        {
            ulong maxValue = (ulong)Math.Sqrt(value);
            for (ulong i = 1; i < maxValue; i++)
            {
                yield return i;
            }
        }
    }
}
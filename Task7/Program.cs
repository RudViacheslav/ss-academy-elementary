﻿// <copyright file="Program.cs"  company="SoftServe">
// Copyright (c) 2018 Viacheslav Rud
// </copyright>

namespace Task7
{
    using System;

    using InputHandling;

    // Программа выводит ряд натуральных чисел через запятую, квадрат которых меньше заданного n.
    // Программа запускается через вызов главного класса с параметрами.

    /// <summary>
    ///     Contains Main.
    /// </summary>
    public class Program
    {
        /// <summary>
        ///     Defines the entry point of the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public static void Main(string[] args)
        {
            var ynHandler = new InputHandlerYesNo("Enter another value?");
            var intHandler = new InputHandlerUlong("Type value:");
            do
            {
                ulong value = intHandler.GetInput();
                var series = SeriesGetter.GetSeriesOfNumbersSquareOfWhichLowerThanValue(value);
                foreach (var item in series)
                {
                    Console.WriteLine("{0},",item);
                }
            }
            while (ynHandler.GetInput());
        }
    }
}